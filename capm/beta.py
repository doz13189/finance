# append import path
import os, sys

corrent_dir = os.getcwd()
os.chdir("../")
sys.path.append(os.getcwd())
import scraping_stock
os.chdir(corrent_dir)

import pandas as pd
from scipy import stats


class FinanceCAPM():

	def set_list(self):

		etf_df = pd.read_csv("etf_list.csv")
		etf_list = [i for i in etf_df["code"].values]

		return etf_list

	# correct_type is 2 type. "local" or "scrape" # correct_typeは"local" or "scrape"が選べる
	# stock_list is a list type. # stock_listはリスト型
	def set_data(self, correct_type, stock_list, days, term):
		correct_type = correct_type

		if correct_type == "scrape":
			print("scrape")
			for s in stock_list:
				scraping_stock.scraping(s, days)
		
		elif correct_type == "local":
			pass

		master_df = pd.DataFrame({})
		for s in stock_list:
			df = pd.read_csv("../data/code_{0}.csv".format(s))
			df = df.sort_values("Date", ascending=True)

			tmp_df = df.loc[:, ["Date", "Adj Close"]]
			tmp_df.columns = ["Date_{0}".format(s), "Close_{0}".format(s)]

			master_df = pd.concat([master_df, tmp_df["Close_{0}".format(s)]], axis=1)
			# print("master_df length : ", len(master_df))
			print(master_df)

		return master_df

	def calc_beta(self, master_df, stock_list):
		# Calculate annual average return # 年率を計算する前に日率を計算
		# .pct_change：Rate of change # リターンを計算する便利な関数
		# print(master_df.tail(10))
		returns_daily = master_df.pct_change()
		returns_annual = returns_daily.mean() * 100
		print("returns_annual")
		print(returns_annual)

		beta_list = []
		for s in stock_list:
			tmp_df = returns_daily.loc[:, ["Close_998405", "Close_{0}".format(s)]]
			x = [i[0] for i in tmp_df.iloc[:, [0]].values]
			y = [i[0] for i in tmp_df.iloc[:, [1]].values]
			slope, intercept, r_value, p_value, std_err = stats.linregress(x[1:len(x)], y[1:len(y)])
			print("β : ", slope)

			# β = 証券iと市場ポートフォリオの相関 × 証券i の標準偏差 / 市場ポートフォリオの標準偏差			
			tmp_df = returns_daily.loc[:, ["Close_998405", "Close_{0}".format(s)]]
			corr = tmp_df.corr().iloc[0, 1]
			std_i = returns_daily["Close_{0}".format(s)].std()
			std_m = returns_daily["Close_998405"].std()
			# print("Calculate : ", "{0} * {1} / {2}".format(std_i, corr, std_m))
			beta = std_i * corr / std_m
			# print("beta : ", beta)
			beta_list.append(beta)

		# beta_df = pd.DataFrame({"code" : stock_list, "beta" : beta_list})
		beta_df = pd.DataFrame({"beta" : beta_list})
		print(beta_df)


	def calcreturn(self):
		print("")


if __name__ == '__main__':
	fin = FinanceCAPM()
	correct_type = "local"
	correct_type = "scrape"
	days = 250
	term = "m"
	stock_list = fin.set_list()
	stock_list = [998405, 3778]
	master_df = fin.set_data(correct_type, stock_list, days)
	# print(master_df.head(10))
	# print(master_df.tail(10))

	fin.capm(master_df, stock_list)
