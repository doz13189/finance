import pandas as pd
import numpy as np
import matplotlib
import os, sys

corrent_dir = os.getcwd()
os.chdir("../")
sys.path.append(os.getcwd())
import scraping_stock
os.chdir(corrent_dir)

matplotlib.use("Agg")
import matplotlib.pyplot as plt


class Finance():

	# correct_type is 2 type. "local" or "scrape" # correct_typeは"local" or "scrape"が選べる
	# stock_list is a list type. # stock_listはリスト型
	def set_data(self, correct_type, stock_list, days):
		correct_type = correct_type

		if correct_type == "scrape":
			for s in stock_list:
				scraping_stock.scraping(s, days)
		
		elif correct_type == "local":
			pass

		master_df = pd.DataFrame({})
		for s in stock_list:
			df = pd.read_csv("../data/code_{0}.csv".format(s))
			df = df.sort_values("Date", ascending=True)
			# print(df.tail(10))

			tmp_df = df.loc[:, ["Date", "Adj Close"]]
			tmp_df.columns = ["Date_{0}".format(s), "Close_{0}".format(s)]

			master_df = pd.concat([master_df, tmp_df["Close_{0}".format(s)]], axis=1)

		return master_df


	# Modern Portfolio Theory
	def mpt(self, master_df):
		# Calculate annual average return # 年率を計算する前に日率を計算
		# .pct_change：Rate of change # リターンを計算する便利な関数
		# print(master_df.tail(10))
		returns_daily = master_df.pct_change()
		# print(returns_daily.tail(10))

		# Convert to annual rate. working day is 250/365. # 年率に変換. 250は年間の市場が動いている日数.
		returns_annual = returns_daily.mean() * 250
		print("returns_annual")
		print(returns_annual)

		# Calculate covariance # 各々のリターンから共分散を計算
		cov_daily = returns_daily.cov()
		cov_annual = cov_daily * 250
		# print("cov_annual")
		# print(cov_annual)

		print(returns_daily.corr())

		# Define list of returns, volatilities, ratiosm, sharpe ratio # 諸々のリスト定義
		port_returns = []
		port_volatility = []
		stock_weights = []
		sharpe_ratio = []

		# Choose a prime number. # seedは素数を選ぼう
		np.random.seed(101)

		# Number of stocks to be combined # 組み入れる株の数
		num_assets = len(stock_list)

		# Number of trials of portfolio creation pattern # ポートフォリオ作成パターンの試行回数
		num_portfolios = 50000

		# Randomly calculate portfolio risks and returns # 様々な銘柄の比率でのポートフォリオのリターンとリスクを計算
		for single_portfolio in range(num_portfolios):

			# Determination of the ratio of stocks by random number # 銘柄の比率を乱数で決定
			weights = np.random.random(num_assets)
			weights /= np.sum(weights)

			# Calculate expected return on portfolio # ポートフォリオの期待リターンを計算
			returns = np.dot(weights, returns_annual)
			# print("returns : ", returns)

			# Calculate portfolio volatility # ポートフォリオのボラティリティを計算
			volatility = np.sqrt(np.dot(weights.T, np.dot(cov_annual, weights)))
			# print("volatility : ", volatility)

			# Calculate sharp ratio
			sharpe = returns / volatility
			sharpe_ratio.append(sharpe)

			# Store calculated values in list # 計算値をリストに格納
			port_returns.append(returns)
			port_volatility.append(volatility)
			stock_weights.append(weights)

		# a dictionary for Returns and Risk values of each portfolio # 辞書型に格納
		portfolio = {"Returns": port_returns,
					"Volatility": port_volatility,
					"Sharpe Ratio" : sharpe_ratio}

		# Add ratio data # 計算したポートフォリオのリターンとリスクに、比率のデータを加える
		for counter,symbol in enumerate(stock_list):
		    portfolio[str(symbol) + " Weight"] = [Weight[counter] for Weight in stock_weights]

		# Convert to DataFrame # PandasのDataFrameに変換
		df = pd.DataFrame(portfolio)

		# Done # データフレーム完成
		column_order = ["Returns", "Volatility", "Sharpe Ratio"] + [str(stock)+" Weight" for stock in stock_list]
		df = df[column_order]

		return df

	def efficient(self, portfolio):
		df = portfolio.sort_values("Sharpe Ratio", ascending=True)
		print(df.head(1))


	def visualization(self, portfolio):
		# look an efficient frontier # 50000パターンのポートフォリオを可視化して、効率的フロンティアを探る
		plt.style.use("seaborn")
		portfolio.plot.scatter(x="Volatility", y="Returns", c="Sharpe Ratio", cmap="RdYlGn", edgecolors="black", figsize=(10, 8), grid=True)
		plt.xlim([0,1])
		plt.xlabel("Volatility (Std. Deviation)")
		plt.ylabel("Expected Returns")
		plt.title("Efficient Frontier")
		plt.savefig("image.png")


if __name__ == '__main__':
	fin = Finance()

	# stock_list = [3416, 3665, 3654, 3199]
	# stock_list = [4689, 9613] # 4689 = yahoo, 9613 = NTT Data
	stock_list = [9719, 3626, 9613, 4689, 9984] # 9719 = SCSK, 3626 = TIS, 9984 = SoftBank
	# stock_list = [4689, 9984] # 9719 = SCSK, 3626 = TIS, 9984 = SoftBank
	# stock_list = [9201, 7203] # 9201 = JAL, 7203 = TOYOTA 
	# stock_list = [9613, 9719]
	# correct_type = "scrape"
	correct_type = "local"
	days = 180
	master_df = fin.set_data(correct_type, stock_list, days)
	portfolio = fin.mpt(master_df)
	fin.efficient(portfolio)
	fin.visualization(portfolio)