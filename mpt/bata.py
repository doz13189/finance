import pandas as pd
import numpy as np
import matplotlib
import datetime
import time


matplotlib.use("Agg")
import matplotlib.pyplot as plt


def scraping(stock, days):

	end = datetime.date.today()
	start = end - datetime.timedelta(days=days)

	base = "http://info.finance.yahoo.co.jp/history/?code={0}.T&{1}&{2}&tm={3}&p={4}"

	start_g = str(start)
	start_g = start_g.split("-")
	start_g = "sy={0}&sm={1}&sd={2}".format(start_g[0], start_g[1], start_g[2])
	end = str(end)
	end = end.split("-")
	end = "ey={0}&em={1}&ed={2}".format(end[0], end[1], end[2])
	page = 1
	term = "d"
	#term = "m"

	result = []
	while True:
		url = base.format(stock, start_g, end, term, page)
		# print(url)
		try:
			df = pd.read_html(url, header=0)
		except ValueError:
			break

		if len(df[1]) == 0:
			break

		result.append(df[1])
		page += 1
		time.sleep(1.0)


	try:
		result = pd.concat(result)
		result.columns = ["Date", "Open", "High", "Low", "Close", "Volume", "Adj Close"]
		
		yahoo_df = pd.DataFrame(result)
		yahoo_df.to_csv("./data/code_" + str(stock) + ".csv", encoding="utf-8")

	except ValueError:
		pass

# stock_list = [9613, 9719]
stock_list = [3626, 9613, 4689, 9984] # 9719 = SCSK, 3626 = TIS, 9984 = SoftBank
# days = 180
# for s in stock_list:
# 	scraping(s, days)

master_df = pd.DataFrame({})
for s in stock_list:
	df = pd.read_csv("./data/code_{0}.csv".format(s))
	df = df.sort_values("Date", ascending=True)
	print(df.tail(10))

	tmp_df = df.loc[:, ["Date", "Adj Close"]]
	tmp_df.columns = ["Date_{0}".format(s), "Close_{0}".format(s)]

	master_df = pd.concat([master_df, tmp_df["Close_{0}".format(s)]], axis=1)

# Calculate annual average return # 年率を計算する前に日率を計算
# .pct_change：Rate of change # リターンを計算する便利な関数
print(master_df.tail(10))
returns_daily = master_df.pct_change()
print(returns_daily.tail(10))

# Convert to annual rate. working day is 250/365. # 年率に変換. 250は年間の市場が動いている日数.
returns_annual = returns_daily.mean() * 250
print("returns_annual")
print(returns_annual)

# Calculate covariance # 各々のリターンから共分散を計算
cov_daily = returns_daily.cov()
cov_annual = cov_daily * 250
# print("cov_annual")
# print(cov_annual)

print(returns_daily.corr())

# Define list of returns, volatilities, ratiosm, sharpe ratio # 諸々のリスト定義
port_returns = []
port_volatility = []
stock_weights = []
sharpe_ratio = []

# Choose a prime number. # seedは素数を選ぼう
np.random.seed(101)

# Number of stocks to be combined # 組み入れる株の数
num_assets = len(stock_list)

# Number of trials of portfolio creation pattern # ポートフォリオ作成パターンの試行回数
num_portfolios = 50000

# Randomly calculate portfolio risks and returns # 様々な銘柄の比率でのポートフォリオのリターンとリスクを計算
for single_portfolio in range(num_portfolios):

	# Determination of the ratio of stocks by random number # 銘柄の比率を乱数で決定
	weights = np.random.random(num_assets)
	weights /= np.sum(weights)

	# Calculate expected return on portfolio # ポートフォリオの期待リターンを計算
	returns = np.dot(weights, returns_annual)
	# print("returns : ", returns)

	# Calculate portfolio volatility # ポートフォリオのボラティリティを計算
	volatility = np.sqrt(np.dot(weights.T, np.dot(cov_annual, weights)))
	# print("volatility : ", volatility)

	# Calculate sharp ratio
	sharpe = returns / volatility
	sharpe_ratio.append(sharpe)

	# Store calculated values in list # 計算値をリストに格納
	port_returns.append(returns)
	port_volatility.append(volatility)
	stock_weights.append(weights)

# a dictionary for Returns and Risk values of each portfolio # 辞書型に格納
portfolio = {"Returns": port_returns,
			"Volatility": port_volatility,
			"Sharpe Ratio" : sharpe_ratio}

# Add ratio data # 計算したポートフォリオのリターンとリスクに、比率のデータを加える
for counter,symbol in enumerate(stock_list):
    portfolio[str(symbol) + " Weight"] = [Weight[counter] for Weight in stock_weights]

# Convert to DataFrame # PandasのDataFrameに変換
df = pd.DataFrame(portfolio)

# Done # データフレーム完成
column_order = ["Returns", "Volatility", "Sharpe Ratio"] + [str(stock)+" Weight" for stock in stock_list]
df = df[column_order]

df = df.sort_values("Sharpe Ratio", ascending=True)
print(df.tail(1))


# look an efficient frontier # 50000パターンのポートフォリオを可視化して、効率的フロンティアを探る
plt.style.use("seaborn")
df.plot.scatter(x="Volatility", y="Returns", c="Sharpe Ratio", cmap="RdYlGn", edgecolors="black", figsize=(10, 8), grid=True)
# df.plot.scatter(x='Volatility', y='Returns', figsize=(10, 8), grid=True)
plt.xlim([0,1])
plt.xlabel("Volatility (Std. Deviation)")
plt.ylabel("Expected Returns")
plt.title("Efficient Frontier")
plt.savefig("image.png")