import pandas as pd
import numpy as np
import datetime
import os, sys

corrent_dir = os.getcwd()
os.chdir("../")
sys.path.append(os.getcwd())
import scraping_stock
os.chdir(corrent_dir)



FFdata = pd.read_csv("F-F_Research_Data_Factors.CSV", 
                     header = 0, names = ["Date","MKT-RF","SMB","HML","RF"], 
                     skiprows=3)


FFdata = FFdata[:1074]
for i in range(len(FFdata)):
	d = FFdata["Date"].iloc[i]
	FFdata["Date"].iloc[i] = datetime.datetime(int(d[0:4]), int(d[4:6]), 1, 0, 0, 0, 0)# print(FFdata.head(10))
# print(FFdata.tail(10))

FFdata = FFdata.loc[FFdata["Date"] >= datetime.datetime(2000, 1, 1, 0, 0, 0, 0)]
FFdata = FFdata.sort_values("Date", ascending=True)


stock = 9613
# scraping_stock.scraping(stock, days=5475 , term="m")
df = pd.read_csv("../data/code_{0}.csv".format(stock))
df = df.dropna(how="any")

date_format = "%Y年%m月"
for i in range(len(df)):
	df["Date"].iloc[i] = datetime.datetime.strptime(df["Date"].iloc[i], date_format)

df = df.sort_values("Date", ascending=True)
tmp_df = df.loc[:, ["Date", "Adj Close"]]

# tmp_df["Return"] = np.log(tmp_df["Adj Close"]) - np.log(tmp_df["Adj Close"].shift(1))
tmp_df["Return"] = tmp_df["Adj Close"].pct_change()

# data = pd.concat([tmp_df["Return"], FFdata], axis=1)
data = pd.merge(FFdata, tmp_df, on="Date")
data.index = data["Date"].values
data = data.drop("Date", axis=1)
data = data.astype(float)
data = data.dropna(how="any")

print(data.head())

data["XReturn"] = data["Return"] * 100 - data["RF"].astype(float)

import statsmodels.api as sm


t = 60
tf_list = []
capm_list = []
for i in range(11, len(data)-t, 12):

	# MKT_RF = data.loc[0:i+t, ["MKT-RF"]].mean().values[0]
	MKT_RF = data.iloc[0:i+t, [0]].mean().values[0]#"MKT-RF"
	SMB = data.iloc[0:i+t, [1]].mean().values[0]#"SMB"
	HML = data.iloc[0:i+t, [2]].mean().values[0]#"HML"
	RF = data.iloc[i+t, [3]].values[0]

	print("MKT-RF : ", MKT_RF, "SMB : ", SMB, "HML : ", HML)

	tmp_data = data.ix[i:i+t]
	print(tmp_data)

	# 3Factor model
	y = tmp_data["XReturn"]
	X = tmp_data.loc[:, ["MKT-RF", "SMB", "HML"]]
	X = sm.add_constant(X)
	model = sm.OLS(y, X)
	results = model.fit()
	# print(results.summary())
	alpha, beta, smb_beta, hml_beta = results.params
	print(alpha, beta, smb_beta, hml_beta)

	expected_return = (beta * MKT_RF) + (smb_beta *  SMB) + (hml_beta * HML) + alpha + RF
	print(expected_return)

	tf_list.append([tmp_data.tail(1).index.values[0], expected_return * 12])

	#CAPM
	X = tmp_data.loc[:, ["MKT-RF"]]
	X = sm.add_constant(X)
	model = sm.OLS(y, X)
	results = model.fit()
	# print(results.summary())
	alpha, beta = results.params
	print(alpha, beta)

	expected_return = (beta * MKT_RF) + alpha + RF
	print(expected_return)

	capm_list.append([tmp_data.tail(1).index.values[0], expected_return * 12])


# print(tf_list)

tf_df = pd.DataFrame({"Date" : [i[0] for i in tf_list],
						"Expected Return" : [i[1] for i in tf_list]})
print("-----3Factor Model-----")
print(tf_df)

capm_df = pd.DataFrame({"Date" : [i[0] for i in capm_list],
						"Expected Return" : [i[1] for i in capm_list]})
print("-----CAPM-----")
print(capm_df)

for i in tf_df["Date"]:
	date_format = "%Y-%m-%d"
	ey = int(i.year) + 1
	ex_df = data.loc[(data.index >= datetime.datetime(ey , 1, 1, 0, 0, 0, 0)) & (data.index <= datetime.datetime(ey, 12, 1, 0, 0, 0, 0)), "Return"]
	print(ey, ex_df.mean() * 100 * 12)