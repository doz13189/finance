import pandas as pd
import datetime
import time
import os


def scraping(stock, days, term):

	end = datetime.date.today()
	end = datetime.date(2015, 12, 31)
	start = end - datetime.timedelta(days=days)
	start = datetime.date(2000, 1, 1)
	print(start, "-", end)
	base = "http://info.finance.yahoo.co.jp/history/?code={0}.T&{1}&{2}&tm={3}&p={4}"

	start_g = str(start)
	start_g = start_g.split("-")
	start_g = "sy={0}&sm={1}&sd={2}".format(start_g[0], start_g[1], start_g[2])
	end = str(end)
	end = end.split("-")
	end = "ey={0}&em={1}&ed={2}".format(end[0], end[1], end[2])
	page = 1
	term = term
	#term = "m"

	result = []
	while True:
		url = base.format(stock, start_g, end, term, page)
		print(url)
		try:
			df = pd.read_html(url, header=0)
		except ValueError:
			break

		if len(df[1]) == 0:
			break

		result.append(df[1])
		page += 1
		time.sleep(1.0)

	# try:
	result = pd.concat(result)
	if stock == 998405:
		result.columns = ["Date", "Open", "High", "Low", "Adj Close"]
	else:
		result.columns = ["Date", "Open", "High", "Low", "Close", "Volume", "Adj Close"]
	
	yahoo_df = pd.DataFrame(result)
	yahoo_df.to_csv("../data/code_" + str(stock) + ".csv", encoding="utf-8")

	# except ValueError:
	# 	pass


if __name__ == '__main__':
	df = scraping(7777, 10)
	print(df)
